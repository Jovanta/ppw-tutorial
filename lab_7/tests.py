from django.test import TestCase
from django.test import Client
from django.urls import resolve
from django.http import HttpRequest
from django.core.paginator import Paginator, EmptyPage, PageNotAnInteger
from django.db.models import Manager
from unittest.mock import patch
from .views import index, response, friend_list, get_friend_list, add_friend, delete_friend, validate_npm, model_to_dict, paginate
from .api_csui_helper.csui_helper import CSUIhelper
from .models import Friend


class Lab7UnitTest(TestCase):
    def test_lab_7_url_is_exist(self):
        response = Client().get('/lab-7/')
        self.assertEqual(response.status_code, 200)


