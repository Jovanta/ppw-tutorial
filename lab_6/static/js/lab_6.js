var awal = "msg-send";
function enterFunc(e) {
    var x = document.getElementById("enter-msg").value;

    if(e.keyCode == 13) {
        if (x!="") {
            var node = document.createElement("DIV");
            if (awal == "msg-send") {
                awal = "msg-receive";
            }
            else {
                awal = "msg-send";
            }
            node.className = awal;
            var textnode = document.createTextNode(x);
            node.appendChild(textnode);
            document.getElementById("result-msg").appendChild(node);
            document.getElementById("enter-msg").value = "";
            e.preventDefault();
        }
    }

}

function hideToggle() {
    var chat = document.getElementById("content-body");
    if (chat.style.display === "none") {
        chat.style.display = "block";
    } else {
        chat.style.display = "none";
    }
}
// Calculator
var print = document.getElementById('print');
var erase = false;

var go = function(x) {
  if (x === 'ac') {
    print.value = "";
    /* implemetnasi clear all */
  } else if (x === 'eval') {
      print.value = Math.round(evil(print.value) * 10000) / 10000;
      erase = true;
  } else {
    print.value += x;
  }
};

function evil(fn) {
  return new Function('return ' + fn)();
}
// END


function changeThis(chosen) {
    $('body').css("backgroundColor", chosen['bcgColor']);
    $('.text-center').css("color", chosen['fontColor']);
}


if (localStorage.getItem('themes') === null) {
    localStorage.setItem('themes', '[{"id":0,"text":"Red","bcgColor":"#F44336","fontColor":"#FAFAFA"},{"id":1,"text":"Pink","bcgColor":"#E91E63","fontColor":"#FAFAFA"},{"id":2,"text":"Purple","bcgColor":"#9C27B0","fontColor":"#FAFAFA"},{"id":3,"text":"Indigo","bcgColor":"#3F51B5","fontColor":"#FAFAFA"},{"id":4,"text":"Blue","bcgColor":"#2196F3","fontColor":"#212121"},{"id":5,"text":"Teal","bcgColor":"#009688","fontColor":"#212121"},{"id":6,"text":"Lime","bcgColor":"#CDDC39","fontColor":"#212121"},{"id":7,"text":"Yellow","bcgColor":"#FFEB3B","fontColor":"#212121"},{"id":8,"text":"Amber","bcgColor":"#FFC107","fontColor":"#212121"},{"id":9,"text":"Orange","bcgColor":"#FF5722","fontColor":"#212121"},{"id":10,"text":"Brown","bcgColor":"#795548","fontColor":"#FAFAFA"}]');
}
var themes = JSON.parse(localStorage.getItem('themes'));

if (localStorage.getItem('selected') === null) {
    localStorage.setItem('selected', JSON.stringify(themes[3]));

}
var selectedTheme = JSON.parse(localStorage.getItem('selected'));
changeThis(selectedTheme);

$(document).ready(function() {
    $('.my-select').select2({'data' : themes}).val(selectedTheme['id']).change();
    $('.apply-button').on('click', function(){
        selectedTheme = themes[$('.my-select').val()];
        changeThis(selectedTheme);
        localStorage.setItem('selected',JSON.stringify(selectedTheme));
    })
});
