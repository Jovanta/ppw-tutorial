from django.shortcuts import render
from django.http import HttpResponse, HttpResponseRedirect, JsonResponse
# Create your views here.

res = {}
def index(request):
    res['author'] = "Jovanta Anugerah Pelawi"
    html = 'lab_8/lab_8.html'
    return render(request, html, res)
